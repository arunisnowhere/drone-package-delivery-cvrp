#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 10 11:25:15 2023

@author: arun
"""

def get_problem_sol_file_pair(n, k):
    problem_fn = 'data/A-VRP/A-n' + str(n) + '-k' + str(k) + '.vrp'
    sol_fn = 'data/A-VRP-sol/opt-A-n' + str(n) + '-k' + str(k)
    write_fn = 'data/A-VRP-my-alpha/latest-A-n' + str(n) + '-k' + str(k)
    return problem_fn, sol_fn, write_fn