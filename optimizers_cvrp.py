import numpy as np
from functools import reduce


#%% Ant Colony Optimization
# = * = * = * = * = * = * = *

alfa = 2
beta = 5
sigm = 3
ro = 0.8
th = 80

def solutionOfOneAnt(cust_nodes, edges, capacity_limit, demand, feromones):
    solution = list()

    while(len(cust_nodes)!=0):
        path = list()
        city = np.random.choice(cust_nodes)
        capacity = capacity_limit - demand[city]
        path.append(city)
        cust_nodes.remove(city)
        while(len(cust_nodes)!=0):
            probabilities = list(map(lambda x: ((feromones[(min(x,city), max(x,city))])**alfa)*((1/edges[(min(x,city), max(x,city))])**beta), cust_nodes))
            probabilities = probabilities/np.sum(probabilities)
            
            city = np.random.choice(cust_nodes, p=probabilities)
            capacity = capacity - demand[city]

            if(capacity>0):
                path.append(city)
                cust_nodes.remove(city)
            else:
                break
        solution.append(path)
    return solution

def rateSolution(solution, edges):
    s = 0
    for i in solution:
        a = 1
        for j in i:
            b = j
            s = s + edges[(min(a,b), max(a,b))]
            a = b
        b = 1
        s = s + edges[(min(a,b), max(a,b))]
    return s

def updateFeromone(feromones, solutions, bestSolution):
    Lavg = reduce(lambda x,y: x+y, (i[1] for i in solutions))/len(solutions)
    feromones = { k : (ro + th/Lavg)*v for (k,v) in feromones.items() }
    solutions.sort(key = lambda x: x[1])
    if(bestSolution!=None):
        if(solutions[0][1] < bestSolution[1]):
            bestSolution = solutions[0]
        for path in bestSolution[0]:
            for i in range(len(path)-1):
                feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = sigm/bestSolution[1] + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
    else:
        bestSolution = solutions[0]
    for l in range(sigm):
        paths = solutions[l][0]
        L = solutions[l][1]
        for path in paths:
            for i in range(len(path)-1):
                feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = (sigm-(l+1)/L**(l+1)) + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
    return bestSolution

def aco_optimizer(cust_nodes, edges, capacity_limit, demand, best_value, nodes_coord, iterations):    
    ants = len(cust_nodes)
    
    nodes_coord_new = {value:index for index, value in np.ndenumerate(nodes_coord)}
    
    # feromones = {(min(a,b),max(a,b)) : 1 for a in nodes_coord.keys() for b in nodes_coord.keys() if a!=b}
    feromones = {(min(a,b),max(a,b)) : 1 for a in nodes_coord_new.keys() for b in nodes_coord_new.keys() if a!=b}
    bestSolution = None
    
    for i in range(iterations):
        solutions = list()
        for _ in range(ants):
            solution = solutionOfOneAnt(cust_nodes.copy(), edges, capacity_limit, demand, feromones)
            solutions.append((solution, rateSolution(solution, edges)))
        bestSolution = updateFeromone(feromones, solutions, bestSolution)
        print(str(i)+":\t"+str(int(bestSolution[1]))+"\t"+str(best_value))        
    return bestSolution


#%% VRPy
# = * = * = * = * = *
# Taken directly from VRPy implementation

def VRPy_optim(nCust, edges, capacity_limit, cust_demand, max_time):
    """
    Parameters
    ----------
    nCust : int
        Number of customers
    edges : 2D numpy array (numpy.ndarray)
        A 2D array of the edge lengths (distances) between all the nodes, including the depot
    capacity_limit : int
        maximum carrying capacity of the vehicle
    cust_demand : dict
        Contains the weight of the package thet needs to be delivered to the customer         
        Each key is the node and the value is the package weight, e.g.,        
        {1:6,2:3,3:7,4:1,5:9}        
    max_tme: int
        maximum time (seconds) for vrpy to use 
        
    Returns
    -------
   Vrpy solution to the VehicleRoutingProblem

    """    

    from networkx import DiGraph
    from vrpy import VehicleRoutingProblem
    G = DiGraph()
    for v in range(1,nCust+1):        
        G.add_edge("Source", v, cost=edges[0,v]) # cost = distance from depot to node/node
        G.add_edge(v, "Sink", cost=edges[v,0]) # cost = distance from node/node to depot
    for v_1 in range(1,nCust+1):    
        for v_2 in [x for x in range(1,nCust+1) if x != v_1]:            
            G.add_edge(v_1, v_2, cost=edges[v_1,v_2])        
    for v in range(1,nCust+1):
        G.nodes[v]["demand"] = cust_demand[v]    
    prob = VehicleRoutingProblem(G, load_capacity = capacity_limit) 
    prob.solve(heuristic_only=True,time_limit=max_time)
    # prob.solve(time_limit=max_time)
    return prob

#%% I-EPOS

# = * = * = * = * = * = * = *
# PLAN SELECTION FOR I-EPOS = *
# = * = * = * = * = * = * = *

# Naive Plan Selection
# (1) pick a random node.
# (2) pick another random node if that node weight + current weight is less than capacity.
# (3) do so with a new drone unless all vertices are picked.

def naive_plan_sel(nPlans, set_vertices, pack_weight, drone_max_cap, distances):
    """
    
    Parameters
    ----------
    nDrones : TYPE
        DESCRIPTION.
    nPlans : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    
    # initialize    
    all_plans_vert = [] # all plans in drone_index, path in vertices
    all_plans_bin = [] # all plans in drone_index, path in binary
    all_plans_cost = [] # all plans with drone_index, cost
    import helper_fns_cvrp as hlp
    import random    

    for counter in range (0,nPlans):
        # initialize all locations
        all_locations = set_vertices.copy()[1:]
        # create dictionary for feasible_pack_weights
        pack_weight_feas = pack_weight.copy()        
        all_drone_paths = {} # collect paths for all drones
        drone_index = 0 # index the drones
        # when a package is delivered to a location, that location is removed from list
        while all_locations: # while there are locations remaining
            drone_index+=1 # the current drone
            picked_weight = 0 # collects the total weight picked by a drone
            drone_path_vert = [] # what is the path of the drone?
            while True:        
                # feasible_locations = all locations that can be picked
                # = all locations where pack_weight < max_cap - picked weight
                feasible_locations = [loc for loc in pack_weight_feas if pack_weight_feas[loc] < (drone_max_cap - picked_weight)]
                if feasible_locations: # if feasible locations exist
                    # find a random location
                    random_loc = random.choice(feasible_locations)                
                    picked_weight = picked_weight + pack_weight[random_loc]                
                    drone_path_vert.append(random_loc)
                    all_locations.remove(random_loc)
                    pack_weight_feas.pop(random_loc)
                else:
                    # not feasible
                    break               
            all_drone_paths[drone_index] = drone_path_vert            
      
      ## if optimal number of drones is known, we must reject plans that exceed
      ## the optimum number of drones. to ensure that nPlans
      ## are generated, also change the above for loop (with counter) to while loop
      ## check if optimum number of drones have been selected
      # if len(all_drone_paths.keys()) == nDrones:
          
        # verify that the no drone has picked up more than capacity weight
        for item in all_drone_paths:
            path = all_drone_paths[item]
            total_weight = 0
            for index in path:
                total_weight = total_weight + pack_weight[index]
            #print(total_weight)
            if total_weight > drone_max_cap:
                print("Error: total weight is", total_weight, "and the selected nodes exceed the maximum capacity of the drone.")        
        
        all_plans_vert.append(all_drone_paths)     
     
    # some plans have n drones, while some plans have n+m drones
    # to equalize this so that all plans have n+m drones,
    # find the maximum number of drones of any plan,
    # and add dummy drones to other plans
    # dummy drones = drones that fly nowhere
    # = drones that visit a node 0, i.e., the depot
    
    # add 0 vertices
    nDrones_max = len(max(all_plans_vert,key=len))
    for ind in range(len(all_plans_vert)):
        while len(all_plans_vert[ind]) < nDrones_max:
            all_plans_vert[ind][len(all_plans_vert[ind])+1] = [0]
    
    for ind in range(len(all_plans_vert)):
        # convert the paths to binary.
        # note that a plan with 0 node will have a binary plan of only 0's
        all_plans_bin.append(hlp.convert_path_vertices_bin(all_plans_vert[ind]))
        # find cost for each drone's complete path
        # note that a plan with 0 node will have a cost of 0
        all_plans_cost.append(hlp.calculate_dist_path(all_plans_vert[ind],distances))
        
    return all_plans_vert, all_plans_bin, all_plans_cost

# Nearest Neighbor Plan Selection
# (1) pick a random node.
# (2) pick nearest possible node that satisfies capacity constraint (new node weight + current weight < capacity).
# (3) do so with a new drone unless all vertices are picked.

def near_neigh_plan_sel(nPlans, set_vertices, pack_weight, drone_max_cap, distances):
    """
    
    Parameters
    ----------
    nDrones : TYPE
        DESCRIPTION.
    nPlans : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """  
           
    # initialize    
    all_plans_vert = [] # all plans in drone_index, path in vertices
    all_plans_bin = [] # all plans in drone_index, path in binary
    all_plans_cost = [] # all plans with drone_index, cost
    import helper_fns_cvrp as hlp
    import random
    import numpy as np
    
    for counter in range (0,nPlans):
        # initialize all locations
        all_locations = set_vertices.copy()[1:]
        # create dictionary for feasible_pack_weights
        pack_weight_feas = pack_weight.copy()        
        all_drone_paths = {} # collect paths for all drones
        drone_index = 0 # index the drones
        # when a package is delivered to a location, that location is removed from list
        while all_locations: # while there are locations remaining
            drone_index+=1 # the current drone
            picked_weight = 0 # collects the total weight picked by a drone
            drone_path_vert = [] # what is the path of the drone?
            while True:        
                # feasible_locations = all locations that can be picked
                # = all locations where pack_weight < max_cap - picked weight
                feasible_locations = [loc for loc in pack_weight_feas if pack_weight_feas[loc] < (drone_max_cap - picked_weight)]                
                if feasible_locations: # if feasible locations exist                    
                    # if this is the first node chosen,                    
                    if len(drone_path_vert) == 0:          
                        # choose the first node randomly
                        loc = random.choice(feasible_locations)
                    else:
                        # find the nearest feasible location
                        feasible_dist = distances[loc,feasible_locations]
                        loc = feasible_locations[np.argmin(feasible_dist)]
                    picked_weight = picked_weight + pack_weight[loc]
                    drone_path_vert.append(loc)           
                    all_locations.remove(loc)
                    pack_weight_feas.pop(loc)
                else:
                    # not feasible
                    break               
            all_drone_paths[drone_index] = drone_path_vert            
      
      ## if optimal number of drones is known, we must reject plans that exceed
      ## the optimum number of drones. to ensure that nPlans
      ## are generated, also change the above for loop (with counter) to while loop
      ## check if optimum number of drones have been selected
      # if len(all_drone_paths.keys()) == nDrones:
          
        # verify that the no drone has picked up more than capacity weight
        for item in all_drone_paths:
            path = all_drone_paths[item]
            total_weight = 0
            for index in path:
                total_weight = total_weight + pack_weight[index]
            #print(total_weight)
            if total_weight > drone_max_cap:
                print("Error: total weight is", total_weight, "and the selected nodes exceed the maximum capacity of the drone.")        
        
        all_plans_vert.append(all_drone_paths)     
     
    # some plans have n drones, while some plans have n+m drones
    # to equalize this so that all plans have n+m drones,
    # find the maximum number of drones of any plan,
    # and add dummy drones to other plans
    # dummy drones = drones that fly nowhere
    # = drones that visit a node 0, i.e., the node
    
    # add 0 vertices
    nDrones_max = len(max(all_plans_vert,key=len))
    for ind in range(len(all_plans_vert)):
        while len(all_plans_vert[ind]) < nDrones_max:
            all_plans_vert[ind][len(all_plans_vert[ind])+1] = [0]
    
    for ind in range(len(all_plans_vert)):
        # convert the paths to binary.
        # note that a plan with 0 node will have a binary plan of only 0's
        all_plans_bin.append(hlp.convert_path_vertices_bin(all_plans_vert[ind]))
        # find cost for each drone's complete path
        # note that a plan with 0 node will have a cost of 0
        all_plans_cost.append(hlp.calculate_dist_path(all_plans_vert[ind],distances))
        
    return all_plans_vert, all_plans_bin, all_plans_cost



#%% Google OR Tools
# = * = * = * = * = *
# Taken directly from Google OR Website

def GoogleOR_print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()}')
    total_distance = 0
    total_load = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        route_distance = 0
        route_load = 0
        while not routing.IsEnd(index):
            node_index = manager.IndexToNode(index)
            route_load += data['demands'][node_index]
            plan_output += ' {0} Load({1}) -> '.format(node_index, route_load)
            previous_index = index
            index = solution.Value(routing.NextVar(index))
            # route_distance += routing.GetArcCostForVehicle(
            #     previous_index, index, vehicle_id)
            from_node = manager.IndexToNode(previous_index)
            to_node = manager.IndexToNode(index)
            distance = data['distance_matrix'][from_node][to_node]
            route_distance += distance
        plan_output += ' {0} Load({1})\n'.format(manager.IndexToNode(index),
                                                 route_load)
        plan_output += 'Distance of the route: {}m\n'.format(route_distance)
        plan_output += 'Load of the route: {}\n'.format(route_load)
        print(plan_output)
        total_distance += route_distance
        total_load += route_load
    print('Total distance of all routes: {}m'.format(total_distance))
    print('Total load of all routes: {}'.format(total_load))
    return total_distance

def GoogleOR_opt(nCust, pack_weight, drone_max_cap, distances, max_time):
    """Solve the CVRP problem."""
    
    from ortools.constraint_solver import routing_enums_pb2
    from ortools.constraint_solver import pywrapcp
    
    # Instantiate the data problem.
    data = {}
    data['num_vehicles'] = nCust
    data['depot'] = 0
    data['demands'] = [0] + [pack_weight[k] for k in sorted(pack_weight)]
    data['vehicle_capacities'] = [drone_max_cap] * nCust
    data['distance_matrix'] = distances
  

    # Create the routing index manager.
    manager = pywrapcp.RoutingIndexManager(len(data['distance_matrix']),
                                           data['num_vehicles'], data['depot'])

    # Create Routing Model.
    routing = pywrapcp.RoutingModel(manager)


    # Create and register a transit callback.
    def distance_callback(from_index, to_index):
        """Returns the distance between the two nodes."""
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = manager.IndexToNode(from_index)
        to_node = manager.IndexToNode(to_index)
        return data['distance_matrix'][from_node][to_node]

    transit_callback_index = routing.RegisterTransitCallback(distance_callback)

    # Define cost of each arc.
    routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)


    # Add Capacity constraint.
    def demand_callback(from_index):
        """Returns the demand of the node."""
        # Convert from routing variable Index to demands NodeIndex.
        from_node = manager.IndexToNode(from_index)
        return data['demands'][from_node]

    demand_callback_index = routing.RegisterUnaryTransitCallback(
        demand_callback)
    routing.AddDimensionWithVehicleCapacity(
        demand_callback_index,
        0,  # null capacity slack
        data['vehicle_capacities'],  # vehicle maximum capacities
        True,  # start cumul to zero
        'Capacity')

    # Setting first solution heuristic.
    search_parameters = pywrapcp.DefaultRoutingSearchParameters()
    search_parameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
    search_parameters.local_search_metaheuristic = (
        routing_enums_pb2.LocalSearchMetaheuristic.GUIDED_LOCAL_SEARCH)
    search_parameters.time_limit.FromSeconds(1)
    # search_parameters.time_limit.seconds = 60 * 5    
    search_parameters.savings_parallel_routes = True

    # Solve the problem.
    solution = routing.SolveWithParameters(search_parameters)

    return data, manager, routing, solution