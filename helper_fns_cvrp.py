import re
import os

def getData(fileName):
    f = open("datasets/Vrp-Set-XXL/XXL/Antwerp1.vrp", "r")
    content = f.read()
    optimal_value = re.search("Optimal value: (\d+)", content, re.MULTILINE)
    if(optimal_value != None):
        optimal_value = optimal_value.group(1)
    else:
        optimal_value = re.search("Best value: (\d+)", content, re.MULTILINE)
        if(optimal_value != None):
            optimal_value = optimal_value.group(1)
    capacity_limit = re.search("^CAPACITY :  (\d+)$", content, re.MULTILINE).group(1)
    nodes_coord = re.findall(r"^(\d+) (\d+) (\d+)$", content, re.MULTILINE)
    if nodes_coord == []:
        nodes_coord = re.findall(r"(\d+) (\d+) (\d+)$", content, re.MULTILINE)
    demand = re.findall(r"^(\d+) (\d+)$", content, re.MULTILINE)
    if demand == []:
        demand = re.findall(r"^(\d+) (\d+) $", content, re.MULTILINE)
    nodes_coord = {int(a)-1:(int(b),int(c)) for a,b,c in nodes_coord}
    demand = {int(a)-1:int(b) for a,b in demand}
    capacity_limit = int(capacity_limit)
    # optimal_value = int(optimal_value)
    return capacity_limit, nodes_coord, demand

# get data using CVRPLIB
def chooseDataSet(df, folder_path, opt_meth):    
    
    # Get a list of all files in the folder
    files = os.listdir(folder_path)
    # Filter out files with .vrp extension
    vrp_files = [file for file in files if file.endswith(".vrp")]
    vrp_files.sort() # so that smaller sized files are in the beginning
    vrp_files = vrp_files[1:] + [vrp_files[0]] # to move 'X-n1001-k43.vrp' to the last!    
    if opt_meth == 'All':
        files_list = df.iloc[:, 0].tolist()
    else:
        files_list = df.loc[df[opt_meth] != '', 'Dataset'].tolist()
    for vrp_file in vrp_files:        
        if vrp_file not in files_list:                
            # Construct the corresponding .sol file path
            sol_file = vrp_file.replace(".vrp", ".sol")
            sol_file_path = os.path.join(folder_path, sol_file)            
            # Check if the .sol file exists
            if os.path.isfile(sol_file_path):
                print("All okay. New vrp-sol dataset instance pair selected")                
            break    
    
    vrp_file_path = os.path.join(folder_path, vrp_file)
    
    return vrp_file_path, sol_file_path
   
def convert_path_vertices_bin(paths):
    '''
    accepts a dictionary of lists, with the key representing a drone and the 
    list comprising the drone's path as given  by visited locations, 
    and converts into a binary sequence of length of number of locations, 
    where 0 represents an unvisited location and 1 represents 
    a visited location
    
    Parameters
    ----------
    paths: Dictionary
        Contains a list of locations that each drone visits.
        Each key is the drone index and the value is the drone path given
        by the location it visits. e.g.,
        
        {1:[6,10,4],2:[5,3],3:[7,2],4:[1],5:[8,9]}
￼

    Returns
    -------
    all_paths_bin : Dictionary
        DESCRIPTION.
   

    '''
    all_paths_bin = {}    
    nLocations = max(map(max, paths.values()))
    paths_bin_temp = [0]*nLocations # initialize list of binary path
    for key in paths:
        paths_bin = paths_bin_temp.copy()
        for value in paths[key]:
            if value != 0:                
                paths_bin[value-1] = 1                
        all_paths_bin[key] = ','.join(map(str, paths_bin))
    return all_paths_bin


def calculate_dist_path(paths, distances):
    """   
    # find cost for each drone's complete path
    
    Parameters
    ----------
    paths : TYPE
        DESCRIPTION.

    Returns
    -------
    all_paths_dist : TYPE
        DESCRIPTION.

    """
    all_paths_dist = {}
    for key in paths:
        path = paths[key]        
        # initalize with distance from warehouse to first destination and
        # last destination to warehouse
        path_distance = distances[0,path[0]]+distances[path[-1],0]
        # now, add distances between every destination
        for i in range(0,len(path)-1):      
            path_distance = path_distance + distances[(path[i],path[i+1])]
        all_paths_dist[key] = path_distance       
    return all_paths_dist