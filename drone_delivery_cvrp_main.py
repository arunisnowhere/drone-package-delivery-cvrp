#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: arun
Mon Jan 16 09:55:37 2023

https://neo.lcc.uma.es/vrp/vrp-instances/capacitated-vrp-instances/
# MILP, OR-Tools, VRPy, ACO, and I-EPOS solutions for the CVRP problem for CVRP instances given in

Acknowledgment:
As starter code for MILP, I used the MILP code provided by Ghassen Askri and Kijun Kim:
https://medium.com/cmsa-algorithm-for-the-service-of-the-capacitated/using-cplex-and-python-for-finding-an-exact-solution-for-the-cvrp-ac789ee0d8c4
https://medium.com/jdsc-tech-blog/capacitated-vehicle-routing-problem-cvrp-with-python-pulp-and-google-maps-api-5a42dbb594c0

As starter code for CVRP, I used the CVRP_ACO provided by Piotrek Konowrocki (https://github.com/pkonowrocki/CVRP_ACO)

"""

#%% INITIALIZE

import numpy as np
import pandas as pd
import optimizers_cvrp as opt
import helper_fns_cvrp as hlp
import time
import vrplib
import os
import math


#%% INPUT DATA 

# # Code to create initialize an empty results  file
# columns = ['Dataset', 'BKS', 'BKS Routes', 'VRPy', 'VRPy Routes', 'GOR', 
#             'GOR Routes', 'ACO', 'ACO Routes','I-EPOS', 'I-EPOS Routes', 
#             '% Difference_VRPy', '% Difference_GOR', '% Difference_ACO', '% Difference_I-EPOS']
# df = pd.DataFrame(columns=columns)
# df.reset_index(drop=True, inplace=True)
# df.to_feather('results/results.feather')

# # #%% Input Data Without Using CVRPLIB
# vrp_file_path = "datasets/Vrp-Set-XXL/XXL/Antwerp1.vrp"

# # fileName = 'datasets/Augerat-et-al./A-VRP/A-n80-k10.vrp'
# capacity_limit, nodes_coord, demand = hlp.getData(vrp_file_path)

# nodes = (list(nodes_coord.keys())) # all nodes including the depot
# cust_nodes = nodes.copy()
# cust_nodes.remove(0) # all nodes, except depot node
# # remove demand of depot (= 0)
# cust_demand = demand.copy()
# cust_demand.pop(0)
# nCust = len(cust_nodes) # no. of customers
# edges_aco = { (min(a,b),max(a,b)) : np.sqrt((nodes_coord[a][0]-nodes_coord[b][0])**2 + (nodes_coord[a][1]-nodes_coord[b][1])**2) for a in nodes_coord.keys() for b in nodes_coord.keys()}

# edges = np.zeros((nCust+1,nCust+1)) #  nCust+1 because depot is included in the edges
# for i in range(nCust+1):
#     for j in range(nCust+1):           
#         i_coord= nodes_coord[i]
#         j_coord= nodes_coord[j]        
#         edges[i][j] = np.sqrt((i_coord[0]-j_coord[0])**2 + (i_coord[1]-j_coord[1])**2)      

# best_val = 0
# best_routes = 0

df = pd.read_feather('results/results.feather')

#%% Input Data Using CVRPLIB
# Specify the folder path
folder_path = "datasets/Uchoa-et-al./X/"
# folder_path = "datasets/Augerat-et-al./A/"
# Specify the optimization method
opt_method = ['VRPy','GOR','ACO', 'I-EPOS', 'All']
opt_sel = 0
# If opt method is None, then new files will be chosen irrespective of completion status
# If opt method is any method, then new files will be chosen if the optim method has not been used on that file

vrp_file_path, sol_file_path = hlp.chooseDataSet(df, folder_path, opt_method[opt_sel])        
# vrp_file_path = "datasets/Augerat-et-al./A/A-n32-k5.vrp"
# sol_file_path = "datasets/Augerat-et-al./A/A-n32-k5.sol"
# vrp_file_path = "datasets/Uchoa-et-al./X/X-n685-k75.vrp"
# sol_file_path = "datasets/Uchoa-et-al./X/X-n685-k75.sol"
# vrp_file_path = "datasets/Vrp-Set-XXL/XXL/Antwerp1.vrp"
# sol_file_path = "datasets/Vrp-Set-XXL/XXL/Antwerp1.sol"
vrp_file_path = "datasets/Vrp-Set-XXL/XXL/Flanders2.vrp"
sol_file_path = "datasets/Vrp-Set-XXL/XXL/Flanders2.sol"

vrp_file_name = os.path.basename(vrp_file_path)
sol_file_name = os.path.basename(sol_file_path)

# # Perform operations on the pair of files
print("Processing VRP file:", vrp_file_name)
print("Processing SOL file:", sol_file_name)
   
instance = vrplib.read_instance(vrp_file_path)

capacity_limit = instance['capacity']
nodes_coord = instance['node_coord']
nodes = list(range(0,len(nodes_coord)))
cust_nodes = nodes.copy()
cust_nodes.remove(0) # all nodes, except depot node
demand = instance['demand']
cust_demand = {index: item for index, item in enumerate(demand.tolist())}
del cust_demand[0]
nCust = len(cust_nodes) # no. of customers
edges = instance['edge_weight']

solution = vrplib.read_solution(sol_file_path)
best_val = solution['cost']
best_routes = solution['routes']

df = df.append({'Dataset': vrp_file_name, 'BKS': best_val, 'BKS Routes': best_routes}, ignore_index=True)

#%% VRPy
def runVRPy():
    print("##### VRPy #####")
    start_vrpy = time.time()
    max_time = 60
    try:
        vrpy_sol = opt.VRPy_optim(nCust, edges, capacity_limit, cust_demand, max_time)
    except 'problem Not Solved':
        print('problem Not Solved')
        vrpy_sol.best_value = 'Not found'
        vrpy_sol.best_routes = 'Not found'
    end_vrpy = time.time()
    print("Time taken by VRPy", end_vrpy - start_vrpy)
    return vrpy_sol.best_value, vrpy_sol.best_routes
#%% Google OR Tools
def runGOR():
    print("##### Google OR #####")
    start_GOR = time.time()
    max_time = 3600        
    data, manager, routing, solution_GOR = opt.GoogleOR_opt(nCust, cust_demand, capacity_limit, edges, max_time)
    print(data)
    print(data)
    end_GOR = time.time()
    
    # Print solution on console.
    if solution:
        GOR_best_value = opt.GoogleOR_print_solution(data, manager, routing, solution_GOR)
        print("Time taken by Google OR", end_GOR - start_GOR) 
    return GOR_best_value

#%% Ant Colony Optimization
def runACO():
    print("##### ACO #####")
    # start_aco = time.time()
    # iterations = 1000
    # aco_sol = opt.aco_optimizer(cust_nodes, edges_aco, capacity_limit, demand, best_val, nodes_coord, iterations)
    # end_aco = time.time()
    # print("Solution: ", aco_sol)

#%% I-EPOS
def runIEPOS():
    print("##### IEPOS #####")
    epos_plans_valid = pd.DataFrame()
    gen_plans = pd.DataFrame(columns = ['iteration', 'routes', 'cost'])
    counter = 0 
    while epos_plans_valid.empty and counter < 10:
        counter = counter + 1
        print("# = # = # = # = # = # = #")
        print("Counter: ", counter)
        print("# = # = # = # = # = # = #")
        print("Selecting new plans")
        print("# = # = # = # = # = # = #")                
       
        nPlans = 100 # number of plans
        # all_plans_vert = all plans in drone_index, path in nodes
        # all_plans_bin = all plans in drone_index, path in binary
        # all_plans_cost = all plans with drone_index, cost
         
        # naive plan selection = pick nodes randomly until capacity limit, repeat until all nodes are picked
        # all_plans_vert, all_plans_bin, all_plans_cost  = opt.naive_plan_sel(nPlans, set_nodes, pack_weight, drone_max_cap, distances_flying)
        # neighbor plan selection = pick first node, then pick nearest possible node that satisfies capacity constraint (new node weight + current weight < capacity), repeat until all nodes are picked
        all_plans_vert, all_plans_bin, all_plans_cost  = opt.near_neigh_plan_sel(nPlans, nodes, cust_demand, capacity_limit, edges)
                      
        for i in range(nPlans):
            total_cost = sum([value for value in all_plans_cost[i].values()])
            routes = all_plans_vert[i]                    
            new_row = pd.Series({'iteration': iteration, 'routes': routes, 'cost': total_cost})                    
            gen_plans = pd.concat([gen_plans, new_row.to_frame().T], ignore_index=True)
        
        # all the plans have n drones, but in some cases, th1e plans have a dummy drone(s)
        # dummy drones = drones that fly nowhere = drones that visit node 0
        # the maximum number of drones and NOT the optimim. 
        # Optimum will have to be obtained from the output
        nDrones_EPOS_input = len(max(all_plans_vert,key=len))
         
        ## Connect to EPOS (input)
        # move the current file path to the right directory
        import os, os.path
        os.chdir("/home/arun/research/projects/drone-package-delivery-cvrp")
         
        ## delete all the files in the drones dataset folder
        save_path = 'EPOS-0.0.4/datasets/drones'
        for f in os.listdir(save_path):
            os.remove(os.path.join(save_path, f))
         
        ## create new files in the drones dataset folder for input to epos
        for drone_agent in range(nDrones_EPOS_input):    
            file_name = "agent_" + str(drone_agent) + ".plans"
            compl_file_name = os.path.join(save_path, file_name)         
            file = open(compl_file_name,"w")
            for i in range(nPlans):
                file.write('%s:%s\n' % (all_plans_cost[i][drone_agent+1],all_plans_bin[i][drone_agent+1]))
            file.close()
         
        ## create target file
        import csv
        file_name2 = os.path.join(save_path, "ones.target")
        with open(file_name2 , 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',')
            spamwriter.writerow(['1'] * nCust)
        
        nSimulations = 100
        nIterations = 20
        # epos config properties that can be modified
        epos_prop = {'numPlans': nPlans, 'numAgents': nDrones_EPOS_input, 'planDim': nCust, 'numSimulations': nSimulations,'numIterations': nIterations}
        
        ## modify the config file
        file = open("EPOS-0.0.4/conf/epos.properties", "r")
        replacement = ""
        changes = ""
        # using the for loop
        for line in file:
            line = line.strip()
            for key in epos_prop:
                if key in line:  
                    changes = line.replace(line, key + "=" + str(epos_prop[key]))            
                    break
                else:
                    changes = line    
            replacement = replacement + changes + "\n"
        
        file.close()
        # opening the file in write mode
        fout = open("EPOS-0.0.4/conf/epos.properties", "w")
        fout.write(replacement)
        fout.close()        
        
        ## move older output files to "archives" folder, if they exist.
        #target_dir = '/home/arun/research/projects/drone-package-delivery-cvrp/EPOS-0.0.4/output/archives'
        os.chdir("/home/arun/research/projects/drone-package-delivery-cvrp/EPOS-0.0.4/output")
        #CODE TO BE CHANGED: #if os.path.exists("drones_*"):
        os.system('mv -b drones_*/ archives')
        
        ## Run EPOS
        os.chdir("/home/arun/research/projects/drone-package-delivery-cvrp/EPOS-0.0.4/")
        os.system('java -jar IEPOS-Tutorial.jar')
        
        ## switch to the solutions folder
        output_dir = '/home/arun/research/projects/drone-package-delivery-cvrp/EPOS-0.0.4/output/'
        file_names = sorted(os.listdir(output_dir))
        os.chdir(os.path.join(output_dir, file_names[1]))
        
        ## Extract the selected plans from "selected-plans.csv" file
        row_num = list(range(1,nSimulations*nIterations+1))
        for i in range(1,nSimulations+1):
            row_num.remove(nIterations*i)
        if os.path.isfile('selected-plans.csv'):
            epos_plans = pd.read_csv('selected-plans.csv', skiprows = row_num)                
            # epos_plans = dataframe with selected plans as plan indexes (per agent) after each simulation
            
            ## Still need to find the actual nodes associated with the plan indexes and solution cost
            # append an initial cost of each solution (set of drone agent plans)
            epos_plans["Cost"] = 0
            # create epos_plans_bin = dataframe with selected plans as binary sequence
            epos_plans_bin = epos_plans.copy()
            
            for simulation in range(nSimulations):
                for drone_agent in range(1,nDrones_EPOS_input+1):
                    drone_plan = epos_plans.loc[simulation,str('agent-'+str(drone_agent-1))]
                    epos_plans_bin.loc[simulation,str('agent-'+str(drone_agent-1))] = all_plans_bin[drone_plan][drone_agent]
                    epos_plans.loc[simulation,'Cost'] = epos_plans.loc[simulation,'Cost'] + all_plans_cost[drone_plan][drone_agent]
                    epos_plans_bin.loc[simulation,'Cost'] = epos_plans.loc[simulation,'Cost']
    
             # all the plans are not valid because all the limits are not met. criteria that are not met:
                # (1) all nodes must be visited.
                # (2) all nodes must be visited only once.                
            epos_plans_valid = epos_plans_bin.copy()
            
            for simulation in range(nSimulations):     
                all_locations = []
                for drone_agent in range(1,nDrones_EPOS_input+1):
                    plan = epos_plans_bin.loc[simulation,str('agent-'+str(drone_agent-1))].replace(',', '')                
                    visited_locs_drone = [i+1 for i in range(len(plan)) if plan[i] == '1']        
                    all_locations.append(visited_locs_drone)
                all_locations = sum(all_locations, [])
                # if duplicates
                if len(all_locations) != len(set(all_locations)):
                    # remove duplicates
                    all_locations = list(set(all_locations))
                all_locations.sort()
                if all_locations != list(range(1,nCust+1)):
                # if len(all_locations) < nCust-(0.95*nCust): # for n% locations less than all locations                                                       
                    epos_plans_valid = epos_plans_valid.drop([simulation])        
            
    # return epos_plans_valid
    return epos_plans_valid, gen_plans

#%% Switch Optimization Methods
if opt_method[opt_sel] == 'VRPy': # VRPy
    vrpy_best_val, vrpy_best_routes_tmp = runVRPy()
    vrpy_best_routes  = [v for v in vrpy_best_routes_tmp .values()]
    vrpy_best_routes = [[item for item in v if item not in ['Source', 'Sink']] for v in vrpy_best_routes_tmp.values()]
    print("VRPY Solution:", vrpy_best_val)
    print("Best Value:", best_val)
    df.loc[df['Dataset'] == vrp_file_name, 
            ['VRPy', '% Difference_VRPy']] = [int(vrpy_best_val), ((vrpy_best_val-best_val)/best_val)*100]
    # Locate the row where the value in 'Dataset' matches vrp_file_name
    mask = df['Dataset'] == vrp_file_name
    row_index = df[mask].index[0]
    # use the at accessor
    df.at[row_index, 'VRPy Routes'] = vrpy_best_routes
    
elif opt_method[opt_sel] == 'GOR': # GOR        
    GOR_best_val = runGOR()        
    print("GOR Solution:", GOR_best_val)
    print("Best Value:", best_val)
    df.loc[df['Dataset'] == vrp_file_name,
            ['GOR', '% Difference_GOR']] = [int(GOR_best_val), ((GOR_best_val-best_val)/best_val)*100]
elif opt_method[opt_sel] == 'ACO': # ACO
    runACO()
elif opt_method[opt_sel] == 'I-EPOS': # I-EPOS
    start_epos = time.time()
    epos_plans_all = pd.DataFrame()
    gen_plans_all = pd.DataFrame()
    epos_best_val = []
    gen_plans_best_val = []
    for iteration in range(1):
        print("# = # = # = # = # = # = #")
        print("Iteration: ", iteration)
        print("# = # = # = # = # = # = #")
        epos_plans, gen_plans = runIEPOS()
        epos_plans_all = pd.concat([epos_plans_all, epos_plans])
        gen_plans_all = pd.concat([gen_plans_all, gen_plans])
        epos_best_val.append(epos_plans_all["Cost"].min())
        gen_plans_best_val.append(gen_plans_all["cost"].min())
    end_epos = time.time()
    print("Time taken by EPOS", (end_epos - start_epos)/60)
    
    # vrpy_best_routes  = [v for v in vrpy_best_routes_tmp .values()]
    # vrpy_best_routes = [[item for item in v if item not in ['Source', 'Sink']] for v in vrpy_best_routes_tmp.values()]    
    print("I-EPOS Solution:", epos_best_val)
    print("Uncoordinated Solution:", gen_plans)
    print("Best Value:", best_val)
    # df.loc[df['Dataset'] == vrp_file_name, 
    #         ['IEPOS', '% Difference_IEPOS']] = [int(epos_best_val), ((epos_best_val-best_val)/best_val)*100]    
    # print(gen_plans["cost"].min())
    # print(epos_best_val)
    # # Locate the row where the value in 'Dataset' matches vrp_file_name
    # mask = df['Dataset'] == vrp_file_name
    # row_index = df[mask].index[0]
    # # use the at accessor
    # df.at[row_index, 'VRPy Routes'] = vrpy_best_routes        
    
elif opt_method[opt_sel] == 'All': # All
    runVRPy()
    runGOR()
    runACO()

os.chdir("/home/arun/research/projects/drone-package-delivery-cvrp")
df.columns = df.columns.astype(str)    
df.to_feather('results/results.feather')
import beepy
beepy.beep(sound='ping')

#%% Results