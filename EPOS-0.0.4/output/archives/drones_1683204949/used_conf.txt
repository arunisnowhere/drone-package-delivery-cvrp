CONFIGURATION:
dataset = drones
output = /home/arun/research/projects/drone-package-delivery-cvrp/EPOS-0.0.4/output/drones_1683204949
==============
numSimulations = 100
dataset = drones
numAgents = 5
numPlans = 100
planDim = 31
numIterations = 20
numChildren = 2
--------------
alpha = 0.0
beta = 0.0
global cost function = func.RMSECostFunction@3dfc5fb8
local cost function = preference local cost function
goal signal = 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
--------------
permutationID = 0
reorganizationSeed = 0
permutationSeed = 0
permutationFile = null
reorganizationPeriod = 3
memorizationOffset = 5
reorganizationStrategy = NEVER
convergenceTolerance = 0.5
--------------
loggingLevel = SEVERE
Selected Loggers: LocalCostMultiObjectiveLogger, GlobalResponseVectorLogger, SelectedPlanLogger, TerminationLogger, GlobalComplexCostLogger, UnfairnessLogger, GlobalCostLogger, WeightsLogger, ReorganizationLogger, PositionLogger, PlanFrequencyLogger
