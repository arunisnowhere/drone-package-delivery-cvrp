#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 14 09:58:06 2022

@author: arun
"""
def convert_path_vertices_bin(paths):
    '''
    accepts a dictionary of lists, with the key representing a drone and the 
    list comprising the drone's path as given  by visited locations, 
    and converts into a binary sequence of length of number of locations, 
    where 0 represents an unvisited location and 1 represents 
    a visited location
    
    Parameters
    ----------
    paths: Dictionary
        Contains a list of locations that each drone visits.
        Each key is the drone index and the value is the drone path given
        by the location it visits. e.g.,
        
        {1:[6,10,4],2:[5,3],3:[7,2],4:[1],5:[8,9]}
￼

    Returns
    -------
    all_paths_bin : Dictionary
        DESCRIPTION.
   

    '''
    all_paths_bin = {}    
    nLocations = max(map(max, paths.values()))
    paths_bin_temp = [0]*nLocations # initialize list of binary path
    for key in paths:
        paths_bin = paths_bin_temp.copy()
        for value in paths[key]:
            if value != 0:                
                paths_bin[value-1] = 1                
        all_paths_bin[key] = ','.join(map(str, paths_bin))
    return all_paths_bin


def calculate_dist_path(paths, distances):
    """   
    # find cost for each drone's complete path
    
    Parameters
    ----------
    paths : TYPE
        DESCRIPTION.

    Returns
    -------
    all_paths_dist : TYPE
        DESCRIPTION.

    """
    all_paths_dist = {}
    for key in paths:
        path = paths[key]        
        # initalize with distance from warehouse to first destination and
        # last destination to warehouse
        path_distance = distances[0,path[0]]+distances[path[-1],0]
        # now, add distances between every destination
        for i in range(0,len(path)-1):      
            path_distance = path_distance + distances[(path[i],path[i+1])]
        all_paths_dist[key] = path_distance       
    return all_paths_dist


def calc_driving_dist(input_df):
    """
    calculate driving distance between two points using google api

    Parameters
    ----------
    input_df : TYPE
        DESCRIPTION.

    Returns
    -------
    distances_driving : TYPE
        DESCRIPTION.

    """
   
    import numpy as np
    import googlemaps
    
    API_KEY = 'AIzaSyAm3GGO4cINQOLHAUhonuMBn3OXQHZcxuM'    
    googlemaps = googlemaps.Client(key=API_KEY)
    
    # create a latitude,longiture column to use google apu
    input_df['latitude-longitude'] = '0'
    for i in range(len(input_df)):
       input_df['latitude-longitude'].iloc[i] = str(input_df.latitude[i]) + ',' + str(input_df.longitude[i])
    
    # initialize
    distances_driving = np.zeros((len(input_df),len(input_df)))       
        
    for i in range(len(input_df)):
        for j in range(len(input_df)):                 
            # append distance to result list           
            google_maps_api_result = googlemaps.directions(input_df['latitude-longitude'].iloc[i],
                                                           input_df['latitude-longitude'].iloc[j],
                                                          mode = 'driving')   
            distances_driving[i][j] = google_maps_api_result[0]['legs'][0]['distance']['value']

    return distances_driving

def calc_flying_dist(input_df):
    """
    # calculate flying distance between two points using Haversine   

    Parameters
    ----------
    input_df : TYPE
        DESCRIPTION.

    Returns
    -------
    distances_flying : TYPE
        DESCRIPTION.

    """

    from geopy.distance import geodesic
    import numpy as np
    
    distances_flying = np.zeros((len(input_df),len(input_df)))
    
    for i in range(len(input_df)):
        for j in range(len(input_df)):            
            # calculate distance of all pairs
            location1 = (input_df['latitude'].iloc[i], input_df['longitude'].iloc[i])
            location2 = (input_df['latitude'].iloc[j], input_df['longitude'].iloc[j])
            # append distance to result list                
            distances_flying[i][j] = geodesic(location1, location2).meters # meters

    return distances_flying

# = * = * = * = * = * = * = *
# Ant Colony Optimization = *
# = * = * = * = * = * = * = *

def solutionOfOneAnt(alfa, beta, vertices, edges, capacityLimit, demand, feromones):    
    import numpy
    
    
    solution = list()

    while(len(vertices)!=0):
        path = list()
        city = numpy.random.choice(vertices)
        capacity = capacityLimit - demand[city]
        path.append(city)
        vertices.remove(city)
        while(len(vertices)!=0):
            probabilities = list(map(lambda x: ((feromones[(min(x,city), max(x,city))])**alfa)*((1/edges[(min(x,city), max(x,city))])**beta), vertices))
            probabilities = probabilities/numpy.sum(probabilities)
            
            city = numpy.random.choice(vertices, p=probabilities)
            capacity = capacity - demand[city]

            if(capacity>0):
                path.append(city)
                vertices.remove(city)
            else:
                break
        solution.append(path)
    return solution

def rateSolution(solution, edges):
    s = 0
    for i in solution:
        a = 1
        for j in i:
            b = j
            s = s + edges[(min(a,b), max(a,b))]
            a = b
        b = 1
        s = s + edges[(min(a,b), max(a,b))]
    return s

def updateFeromone(ro, th, sigm, feromones, solutions, bestSolution):
    
    from functools import reduce
    
    Lavg = reduce(lambda x,y: x+y, (i[1] for i in solutions))/len(solutions)
    feromones = { k : (ro + th/Lavg)*v for (k,v) in feromones.items() }
    solutions.sort(key = lambda x: x[1])
    if(bestSolution!=None):
        if(solutions[0][1] < bestSolution[1]):
            bestSolution = solutions[0]
        for path in bestSolution[0]:
            for i in range(len(path)-1):
                feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = sigm/bestSolution[1] + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
    else:
        bestSolution = solutions[0]
    for l in range(sigm):
        paths = solutions[l][0]
        L = solutions[l][1]
        for path in paths:
            for i in range(len(path)-1):
                feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = (sigm-(l+1)/L**(l+1)) + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
    return bestSolution



# =============================================================================
# def aco_model(ants, capacityLimit, graph, demand, optimalValue):
#     import math
#     import random
#     from functools import reduce
#     import sys
#     import getopt
#     import numpy
#     alfa = 2
#     beta = 5
#     sigm = 3
#     ro = 0.8
#     th = 80
#     iterations = 10
#     
#     #ants = 22
#     
#     #capacityLimit = 6000
#     #optimalValue = 0
#     #set_nodes = [i for i in range(2, ants+1)]
#     #graph = {i-1:(rnd.randint(100,300),rnd.randint(100,300)) for i in set_nodes}    
#     #demand = {i:rnd.randint(100,capacityLimit) for i in set_nodes}
#     #demand[1] = 0
#     
#     def generateGraph(capacityLimit, graph, demand, optimalValue):
#         vertices = list(graph.keys())    
#         vertices.remove(1)
#         edges = { (min(a,b),max(a,b)) : numpy.sqrt((graph[a][0]-graph[b][0])**2 + (graph[a][1]-graph[b][1])**2) for a in graph.keys() for b in graph.keys()}            
#         feromones = { (min(a,b),max(a,b)) : 1 for a in graph.keys() for b in graph.keys() if a!=b }
#         return vertices, edges, feromones
#     
#     def solutionOfOneAnt(vertices, edges, capacityLimit, demand, feromones):
#         solution = list()
#     
#         while(len(vertices)!=0):
#             path = list()
#             city = numpy.random.choice(vertices)
#             capacity = capacityLimit - demand[city]
#             path.append(city)
#             vertices.remove(city)
#             while(len(vertices)!=0):                
#                 probabilities = list(map(lambda x: ((feromones[(min(x,city), max(x,city))])**alfa)*((1/edges[(min(x,city), max(x,city))])**beta), vertices))                
#                 probabilities = probabilities/numpy.sum(probabilities)
#                 probabilities[numpy.isnan(probabilities)] = 1              
#                 city = numpy.random.choice(vertices, p=probabilities)
#                 capacity = capacity - demand[city]
#     
#                 if(capacity>0):
#                     path.append(city)
#                     vertices.remove(city)
#                 else:
#                     break
#             solution.append(path)
#         return solution
#     
#     def rateSolution(solution, edges):
#         s = 0
#         for i in solution:
#             a = 1
#             for j in i:
#                 b = j
#                 s = s + edges[(min(a,b), max(a,b))]
#                 a = b
#             b = 1
#             s = s + edges[(min(a,b), max(a,b))]
#         return s
#     
#     def updateFeromone(feromones, solutions, bestSolution):
#         Lavg = reduce(lambda x,y: x+y, (i[1] for i in solutions))/len(solutions)
#         feromones = { k : (ro + th/Lavg)*v for (k,v) in feromones.items() }
#         solutions.sort(key = lambda x: x[1])
#         if(bestSolution!=None):
#             if(solutions[0][1] < bestSolution[1]):
#                 bestSolution = solutions[0]
#             for path in bestSolution[0]:
#                 for i in range(len(path)-1):
#                     feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = sigm/bestSolution[1] + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
#         else:
#             bestSolution = solutions[0]
#         for l in range(sigm):
#             paths = solutions[l][0]
#             L = solutions[l][1]
#             for path in paths:
#                 for i in range(len(path)-1):
#                     feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))] = (sigm-(l+1)/L**(l+1)) + feromones[(min(path[i],path[i+1]), max(path[i],path[i+1]))]
#         return bestSolution
# 
# =============================================================================

"""



# = * = * = *
# PLOTS = * =
# = * = * = *

# create the map plotter with the depot latitude, longitude and zoom resolution
gmap = gmplot.GoogleMapPlotter(depot_latitude, depot_longitude, 14, apikey=API_KEY)

# mark the depot
gmap.marker(depot_latitude, depot_longitude, color='red', label = 'Center')

label_list = []
for idx,item in enumerate(cust_data_df.weight[1:]):    
    label_list.append('c_' + str(idx) + ', ' + str(item))
    
# Scatter map
gmap.scatter(cust_data_df.latitude[1:], cust_data_df.longitude[1:], color = 'yellow', face_alpha = 0,
             size = 20, marker = True, label = label_list)

# save it to html
gmap.draw("drone_map_inital_locations.html")
# automatically open in the web browser
import webbrowser
webbrowser.open('drone_map_inital_locations.html')

# Illustrate the solution with connecting lines to show the drone paths
for i,j in active_arcs:    
    gmap.plot([cust_data_df.latitude[i], cust_data_df.latitude[j]],
               [cust_data_df.longitude[i], cust_data_df.longitude[j]], 'cornflowerblue', edge_width = 3.0)

# save it to html
gmap.draw("drone_map_optimal_route.html")
# automatically open in the web browser
import webbrowser
webbrowser.open('drone_map_optimal_route.html')


#Identifing the active arcs.
#active_arcs =[  a for a in A if x[a].solution_value> 0.9]
#plt.scatter(loc_x[1:],loc_y[1:],c='b')
#for i in N:
#    plt.annotate('$d_%d=%d$'%(i,d[i]),(loc_x[i]+2,loc_y[i]))
#    for i,j in active_arcs:
#    #Coloring the active arcs
#        plt.plot([loc_x[i],loc_x[j]],[loc_y[i],loc_y[j]],c='g',alpha=0.3)
#        plt.plot(loc_x[0],loc_y[0],c='r' ,marker='s')
#Plotting the solution
#plt.show()

print(distances_flying[(0,1)]+distances_flying[(1,0)]) # 0 = 0
print(distances_flying[(0,1)]+distances_flying[(1,2)]+distances_flying[(2,0)]) # 1 = 1
print(distances_flying[(0,1)]+distances_flying[(1,3)]+distances_flying[(3,0)]) # 2 = 2
print(distances_flying[(0,1)]+distances_flying[(1,4)]+distances_flying[(4,0)]) # 3 = 3
print(distances_flying[(0,1)]+distances_flying[(1,4)]+distances_flying[(4,5)]+distances_flying[(5,0)]) # 4 = 4
print(distances_flying[(0,1)]+distances_flying[(1,5)]+distances_flying[(5,0)]) # 5 = 5
print(distances_flying[(0,1)]+distances_flying[(1,5)]+distances_flying[(5,4)]+distances_flying[(4,0)]) # 6 = 6
print(distances_flying[(0,2)]+distances_flying[(2,0)]) # 7 = 7
#print(distances_flying[(0,2)]+distances_flying[(2,1)]+distances_flying[(1,0)]) # 8
print(distances_flying[(0,3)]+distances_flying[(3,0)]) # 9 = 8
# print(distances_flying[(0,3)]+distances_flying[(3,1)]+distances_flying[(1,0)]) # 10
print(distances_flying[(0,4)]+distances_flying[(4,0)]) # 11 = 9
#print(distances_flying[(0,4)]+distances_flying[(4,1)]+distances_flying[(1,0)]) # 12
print(distances_flying[(0,4)]+distances_flying[(4,1)]+distances_flying[(1,5)]+distances_flying[(5,0)]) # 13 = 10
print(distances_flying[(0,4)]+distances_flying[(4,5)]+distances_flying[(5,0)]) # 14 = 11
#print(distances_flying[(0,4)]+distances_flying[(4,5)]+distances_flying[(5,1)]+distances_flying[(1,0)]) # 15
print(distances_flying[(0,5)]+distances_flying[(5,0)]) # 16 = 12
#print(distances_flying[(0,5)]+distances_flying[(5,1)]+distances_flying[(1,0)]) # 17
print(distances_flying[(0,5)]+distances_flying[(5,1)]+distances_flying[(1,4)]+distances_flying[(4,0)]) # 18 = 13
#print(distances_flying[(0,5)]+distances_flying[(5,4)]+distances_flying[(4,0)]) # 19
print(distances_flying[(0,5)]+distances_flying[(5,4)]+distances_flying[(4,1)]+distances_flying[(1,0)]) # 20 = 14
"""