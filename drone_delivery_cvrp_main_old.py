#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Started on Mon Sep 6 09:55:37 2021

Acknowledgment:
I used parts of the MILP code provided by Ghassen Askri and Kijun Kim:
https://medium.com/cmsa-algorithm-for-the-service-of-the-capacitated/using-cplex-and-python-for-finding-an-exact-solution-for-the-cvrp-ac789ee0d8c4
https://medium.com/jdsc-tech-blog/capacitated-vehicle-routing-problem-cvrp-with-python-pulp-and-google-maps-api-5a42dbb594c0

@author: arun

"""

#%% INITIALIZE

import numpy as np
#import matplotlib.pyplot as plt
import pandas as pd
#from gmplot import *
import googlemaps
API_KEY = 'AIzaSyAm3GGO4cINQOLHAUhonuMBn3OXQHZcxuM'
googlemaps = googlemaps.Client(key=API_KEY)
import optimizers_cvrp_old as opt
import helper_fns_cvrp_old as hlp
import time

rnd = np.random # generate random numbers.
rnd.seed(0) # make the same set of numbers appear with every call to random


#%% INPUT DATA

# number of customers
nCust = 15 # +1 warehouse
# maximum drone capacity
drone_max_cap = 15

# The set of customers without the warehouse = packages to be delivered
set_packages = [i for i in range(1, nCust+1)]

# Set of all the nodes including the warehouse. Warehouse = node 0
set_nodes = [0] + set_packages

pack_weight = {i:rnd.randint(1,drone_max_cap) for i in set_packages}

# set warehouse latitude and longitude
# Lappeenranta Matkakeskus
warehouse_latitude = 61.04839718811693
warehouse_longitude = 28.19494674254105

# Bangalore Airport
#warehouse_latitude = 13.20100598279671
#warehouse_longitude = 77.66123382884508

# create a dataframe with random customer locations and package weights
cust_data_df = pd.DataFrame({"latitude":np.random.normal(warehouse_latitude, 0.07, nCust), 
                  "longitude":np.random.normal(warehouse_longitude, 0.07, nCust), 
                   "weight":np.array(list(pack_weight.values()))})

# =============================================================================
# cust_data_df.index +=1
# 
# from sklearn.cluster import DBSCAN
# coords = cust_data_df[["latitude", "longitude"]]
# kms_per_radian = 6371.0088
# epsilon = 0.8 / kms_per_radian
# db = DBSCAN(eps=epsilon, min_samples=5, algorithm='ball_tree', metric='haversine').fit(np.radians(coords))
# cluster_labels = db.labels_
# num_clusters = len(set(cluster_labels))
# #clusters = pd.Series([coords[cluster_labels == n] for n in range(num_clusters)])
# clusters = pd.Series([coords[cluster_labels == n] for n in cluster_labels])
# print('Number of clusters: {}'.format(num_clusters))
# #cluster_dict = {i: coords[cluster_labels==i] for i in range(num_clusters)}
# cluster_dict = {i: coords[cluster_labels==i] for i in cluster_labels}  
# sol = {}
# for cluster_id in cluster_dict.keys():
#     sol[cluster_id]=len(cluster_dict[cluster_id])
# print(max(sol.values()))    
# =============================================================================
# insert the warehouse into the dataframe in the first position
# its package weight = 0
cust_data_df.loc[-1] = [warehouse_latitude, warehouse_longitude, 0]
cust_data_df.index = cust_data_df.index + 1
cust_data_df = cust_data_df.sort_index(inplace=False)

# calculate flying distance between two points (using Haversine)
distances_flying = hlp.calc_flying_dist(cust_data_df) # metres
## calculate driving distance between two points (using google api)
#distances_driving = hlp.calc_driving_dist(cust_data_df)


#%% Ant Colony Optimization
# =============================================================================
# print("##### ACO #####")
# start_aco = time.time()
# sol = {}
# for cluster_id in cluster_dict.keys():
#     sol[cluster_id]=len(cluster_dict[cluster_id])
#     df = pd.DataFrame(cluster_dict[cluster_id])
#     nCust = len(df)    
#     pack_weight_clust = {}
#     for idx in df.index:
#         pack_weight_clust[idx] = pack_weight[idx]
# 
#     df.loc[-1] = [warehouse_latitude, warehouse_longitude]
#     df.index = df.index + 1
#     df = df.sort_index(inplace=False)
#     
#     distances_flying = hlp.calc_flying_dist(df) # metres
# 
#     # Here, warehouse = node 1 and remaining nCust = 2 to nCust+1
#     vertices = [i for i in range(1, nCust+2)]
#     feromones = { (min(a,b),max(a,b)) : 1 for a in vertices for b in vertices if a!=b }
#     # demand is adjusted so that demand of warehouse is added as node 1 with demand 0
#     demand = {}
#     count = 1
#     for key in pack_weight_clust:
#         count += 1
#         demand[count] = pack_weight_clust[key]    
#     demand[1] = 0
#     edges = {(index[0]+1, index[1]+1): v for index, v in np.ndenumerate(distances_flying)}
#     
#     # A collection that contains the weight of the package of each customer
#     #graph = {i+1:(rnd.randint(100,300),rnd.randint(100,300)) for i in set_nodes}
#     #def aco_optimizer(ants, capacityLimit, vertices, demand, edges):
#     
#     aco_sol = opt.aco_optimizer(nCust+1, drone_max_cap, vertices[1:], demand, edges)
#     sol[cluster_id] = aco_sol[-1]
#     
# end_aco = time.time()
# 
# print(sum(sol.values()))
# 
# =============================================================================
#d = {(a-1,b-1) : np.sqrt((graph[a][0]-graph[b][0])**2 + (graph[a][1]-graph[b][1])**2) for a in graph.keys() for b in graph.keys()}
##idx_ar = np.array(list(d.keys()))
#out_shp = idx_ar.max(0)+1
#data = np.array(list(d.values()))
#M = np.zeros(shape=out_shp, dtype=data.dtype)
#M[tuple(idx_ar.T)] = data
#distances_flying = M

#%% MILP Model
print("##### MILP #####")
# Intializing the set of arcs A.
arcs = [(i,j) for i in set_nodes for j in set_nodes if i!=j]
start_milp = time.time()
x_milp, solution_milp = opt.milp_model(set_packages, pack_weight, drone_max_cap, set_nodes, distances_flying, arcs)
end_milp = time.time()

# Identifing the active arcs.
active_arcs = [a for a in arcs if x_milp[a].solution_value> 0.99]

# Identifying the number of drones
nDrones_milp = 0
for arc in active_arcs:
    if arc[0] == 0:
        nDrones_milp = nDrones_milp + 1
# calculating the optimal distance
# converting to dict for easy manipulation
distances_flying_dict = dict(np.ndenumerate(distances_flying))
all_drone_distances = []
active_arcs_temp = active_arcs.copy()
# distance traveled by each drone
for n in range(0,nDrones_milp):
    i,j = active_arcs_temp[n]
    distance_drone = distances_flying_dict[i,j]    
    while j!=0:
        i,j = next(x for x in active_arcs_temp if x[0] == j)
        distance_drone = distance_drone + distances_flying_dict[i,j]
    all_drone_distances.append(distance_drone)

#Printing the solution
print(solution_milp)
print(active_arcs)
print("Number of drones: ", nDrones_milp)
print("distances = ", all_drone_distances)
print("total distance = ", sum(all_drone_distances))
print("Time taken by MILP", end_milp - start_milp)



#%% EPOS Model
 
print("##### EPOS #####")
start_epos = time.time()
epos_plans_valid_all = pd.DataFrame()
for iteration in range(100):
    epos_plans_valid = pd.DataFrame()
    print("# = # = # = # = # = #")
    print("Iteration number", iteration)
    print("# = # = # = # = # = #")
    while epos_plans_valid.empty:
        nPlans = 10 # number of plans
        # all_plans_vert = all plans in drone_index, path in nodes
        # all_plans_bin = all plans in drone_index, path in binary
        # all_plans_cost = all plans with drone_index, cost
         
        print("# = # = # = # = # = # = #")
        print("Selecting new plans")
        print("# = # = # = # = # EPOS #")
        # naive plan selection = pick nodes randomly until capacity limit, repeat until all nodes are picked
        # all_plans_vert, all_plans_bin, all_plans_cost  = opt.naive_plan_sel(nPlans, set_nodes, pack_weight, drone_max_cap, distances_flying)
        # neighbor plan selection = pick first node, then pick nearest possible node that satisfies capacity constraint (new node weight + current weight < capacity), repeat until all nodes are picked
        all_plans_vert, all_plans_bin, all_plans_cost  = opt.near_neigh_plan_sel(nPlans, set_nodes, pack_weight, drone_max_cap, distances_flying)
         
         # all the plans have n drones, but in some cases, the plans have a dummy drone(s)
         # dummy drones = drones that fly nowhere = drones that visit node 0
         # the maximum number of drones and NOT the optimim. 
         # Optimum will have to be obtained from the output
        nDrones_EPOS_input = len(max(all_plans_vert,key=len))
         
         ## Connect to EPOS (input)
         # move the current file path to the right directory
        import os, os.path
        os.chdir("/home/arun/research/projects/drone-package-delivery")
         
# # =============================================================================
# #         ## create text files for input to epos
# #         # warn that new files will replace all the files in the dataset directory
# #         del_warn = input("Warning. New files will replace all the files in the drones dataset dir. Continue? (y/n) ")
# #         if del_warn not in 'yY':
# #             print("Quitting program. Bye!")
# #             import sys
# #             sys.exit()
# # =============================================================================
#         
         ## delete all the files in the drones dataset folder
        save_path = 'EPOS-0.0.2/datasets/drones'
        for f in os.listdir(save_path):
            os.remove(os.path.join(save_path, f))
         
        ## create new files in the drones dataset folder for input to epos
        for drone_agent in range(nDrones_EPOS_input):    
            file_name = "agent_" + str(drone_agent) + ".plans"
            compl_file_name = os.path.join(save_path, file_name)         
            file = open(compl_file_name,"w")
            for i in range(nPlans):
                file.write('%s:%s\n' % (all_plans_cost[i][drone_agent+1],all_plans_bin[i][drone_agent+1]))
            file.close()
         
        ## create target file
        import csv
        file_name2 = os.path.join(save_path, "ones.target")
        with open(file_name2 , 'w', newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',')
            spamwriter.writerow(['1'] * nCust)
        
        nSimulations = 25
        nIterations = 25
        # epos config properties that can be modified
        epos_prop = {'numPlans': nPlans, 'numAgents': nDrones_EPOS_input, 'planDim': nCust, 'numSimulations': nSimulations,'numIterations': nIterations}
        
        ## modify the config file
        file = open("EPOS-0.0.2/conf/epos.properties", "r")
        replacement = ""
        changes = ""
        # using the for loop
        for line in file:
            line = line.strip()
            for key in epos_prop:
                if key in line:  
                    changes = line.replace(line, key + "=" + str(epos_prop[key]))            
                    break
                else:
                    changes = line    
            replacement = replacement + changes + "\n"
        
        file.close()
        # opening the file in write mode
        fout = open("EPOS-0.0.2/conf/epos.properties", "w")
        fout.write(replacement)
        fout.close()
        
        ## Run EPOS
        ## move older output files to "archives" folder, if they exist.
        target_dir = '/home/arun/research/projects/drone-package-delivery/EPOS-0.0.2/output/archives'
        os.chdir("/home/arun/research/projects/drone-package-delivery/EPOS-0.0.2/output")
        #CODE TO BE CHANGED: #if os.path.exists("drones_*"):
        os.system('mv -b drones_*/ archives')
        
        ## run I-EPOS
        os.chdir("/home/arun/research/projects/drone-package-delivery/EPOS-0.0.2/")
        os.system('java -jar IEPOS-Tutorial.jar')
        
        ## switch to the solutions folder
        output_dir = '/home/arun/research/projects/drone-package-delivery/EPOS-0.0.2/output/'
        file_names = sorted(os.listdir(output_dir))
        os.chdir(os.path.join(output_dir, file_names[1]))
        
        ## Extract the selected plans from "selected-plans.csv" file
        row_num = list(range(1,nSimulations*nIterations+1))
        for i in range(1,nSimulations+1):
            row_num.remove(nIterations*i)    
        epos_plans = pd.read_csv('selected-plans.csv', skiprows = row_num)
        # epos_plans = dataframe with selected plans as plan indexes (per agent) after each simulation
        
        ## Still need to find the actual nodes associated with the plan indexes and solution cost
        # append an initial cost of each solution (set of drone agent plans)
        epos_plans["Cost"] = 0
        # create epos_plans_bin = dataframe with selected plans as binary sequence
        epos_plans_bin = epos_plans.copy()
        
        for simulation in range(nSimulations):
            for drone_agent in range(1,nDrones_EPOS_input+1):
                drone_plan = epos_plans.loc[simulation,str('agent-'+str(drone_agent-1))]
                epos_plans_bin.loc[simulation,str('agent-'+str(drone_agent-1))] = all_plans_bin[drone_plan][drone_agent]
                epos_plans.loc[simulation,'Cost'] = epos_plans.loc[simulation,'Cost'] + all_plans_cost[drone_plan][drone_agent]
                epos_plans_bin.loc[simulation,'Cost'] = epos_plans.loc[simulation,'Cost']
#         
        # hist = epos_plans_bin.hist('Cost')
        #epos_plans_bin['Cost'].min()
         
         # all the plans are not valid because all the limits are not met. criteria that are not met:
            # (1) all nodes must be visited.
            # (2) all nodes must be visited only once.
        
        epos_plans_valid = epos_plans_bin.copy()
        
        for simulation in range(nSimulations):     
            all_locations = []
            for drone_agent in range(1,nDrones_EPOS_input+1):
                plan = epos_plans_bin.loc[simulation,str('agent-'+str(drone_agent-1))].replace(',', '')                
                visited_locs_drone = [i+1 for i in range(len(plan)) if plan[i] == '1']        
                all_locations.append(visited_locs_drone)
            all_locations = sum(all_locations, [])
            # if duplicates
            if len(all_locations) != len(set(all_locations)):
                # remove duplicates        
                all_locations = list(set(all_locations))
            all_locations.sort()
            #print(simulation, all_locations)
            if all_locations != list(range(1,nCust+1)):
                epos_plans_valid = epos_plans_valid.drop([simulation])        
        
        epos_plans_valid_all = pd.concat([epos_plans_valid_all , epos_plans_valid])
    print(epos_plans_valid_all)
    print(epos_plans_valid_all["Cost"].min())
         
end_epos = time.time()
print("Time taken by EPOS", end_epos - start_epos)


#%% VRPy

print("##### VRPy #####")
start_vrpy = time.time()
vrpy_sol = opt.VRPy_optim(nCust, distances_flying, drone_max_cap, pack_weight)
end_vrpy = time.time()
print(vrpy_sol.best_value)
print(vrpy_sol.best_routes)
print("Time taken by VRPy", end_vrpy - start_vrpy)

#%% Google OR Tools

print("##### GOR #####")
start_GOR = time.time()
data, manager, routing, solution = opt.GoogleOR_opt(nCust, pack_weight, drone_max_cap, distances_flying.tolist())
end_GOR = time.time()

# Print solution on console.
if solution:
    opt.GoogleOR_print_solution(data, manager, routing, solution)
    print("Time taken by Google OR", end_GOR - start_GOR)


#%% Output

# =============================================================================
# print("##### MILP #####")
# print(solution_milp)
# print(active_arcs)
# print("Number of drones: ", nDrones_milp)
# print("distances = ", all_drone_distances)
# print("total distance = ", sum(all_drone_distances))
# print("##### EPOS #####")
# print(epos_plans_valid_all["Cost"].min())
# print("##### ACO #####")
# print(aco_sol)
# print("##### VRPy #####")
# print(vrpy_sol.best_value)
# print(vrpy_sol.best_routes)
# print("##### GOR #####")
# if solution:
#     opt.GoogleOR_print_solution(data, manager, routing, solution)
# =============================================================================
# # create the map plotter with the depot latitude, longitude and zoom resolution
# gmap = gmplot.GoogleMapPlotter(warehouse_latitude, warehouse_longitude, 14, apikey=API_KEY)

# # mark the depot
# gmap.marker(warehouse_latitude, warehouse_longitude, color='red', label = 'Center')

# label_list = []
# for idx,item in enumerate(cust_data_df.weight[1:]):    
#     label_list.append('c_' + str(idx) + ', ' + str(item))
    
# # Scatter map
# gmap.scatter(cust_data_df.latitude[1:], cust_data_df.longitude[1:], color = 'yellow', face_alpha = 0,
#              size = 20, marker = True, label = label_list)

# # save it to html
# gmap.draw("drone_map_inital_locations.html")
# # automatically open in the web browser
# import webbrowser
# webbrowser.open('drone_map_inital_locations.html')

# # Illustrate the solution with connecting lines to show the drone paths
# for i,j in active_arcs:    
#     gmap.plot([cust_data_df.latitude[i], cust_data_df.latitude[j]],
#                [cust_data_df.longitude[i], cust_data_df.longitude[j]], 'cornflowerblue', edge_width = 3.0)

# # save it to html
# gmap.draw("drone_map_optimal_route.html")
# # automatically open in the web browser
# import webbrowser
# webbrowser.open('drone_map_optimal_route.html')


# #Identifing the active arcs.
# active_arcs =[  a for a in arcs if x[a].solution_value> 0.9]
# plt.scatter(loc_x[1:],loc_y[1:],c='b')
# for i in N:
#     plt.annotate('$d_%d=%d$'%(i,d[i]),(loc_x[i]+2,loc_y[i]))
#     for i,j in active_arcs:
#     #Coloring the active arcs
#        plt.plot([loc_x[i],loc_x[j]],[loc_y[i],loc_y[j]],c='g',alpha=0.3)
#        plt.plot(loc_x[0],loc_y[0],c='r' ,marker='s')
# #Plotting the solution
# plt.show()

